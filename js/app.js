/**
 * Created by Robert on 15 Oct 2019.
 */

// $(function () {
    var element = document.getElementById('ball');
    var angle = 0;
    var x = 0;
    var y = 0;
    var w = (window.innerWidth - 4) / 2;
    var h = (window.innerHeight - 4) / 2;

    function ballCircle() {
        x = w + w * Math.cos(angle * Math.PI / 360);
        y = h + h * Math.sin(angle * Math.PI / 360);

        ball.style.left = x + 'px';
        ball.style.top = y + 'px';

        angle++;
        if (angle > 360) {
            angle = 0;
        }
        setTimeout(ballCircle, 20);
    }

    ballCircle();



// });